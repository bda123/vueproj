import Vue from 'vue'
//import BootstrapVue from 'bootstrap-vue'
import Router from 'vue-router'
import Main from '@/views/Main'
import Product from '@/views/Product'
import Cart from '@/views/Cart'
//import 'bootstrap/dist/css/bootstrap.css'
//import 'bootstrap-vue/dist/bootstrap-vue.css'
//import VueCharts from 'vue-chartjs'

//Vue.use(BootstrapVue);

//import 'vue-awesome/icons'
//import Icon from 'vue-awesome/components/Icon'

Vue.use(Router);

//Vue.component('fa-icon', Icon);

export default new Router({
    routes: [
        {
            path: '/main',
            name: 'Main',
            component: Main
        },
        {
            path: '/product/:data',
            name: 'Product',
            component: Product
        },
        {
            path: '/cart',
            name: 'Cart',
            component: Cart
        }
    ]
})