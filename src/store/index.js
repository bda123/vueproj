import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        searchVal: '',
        navbarDropdownDisplay: 'none'
    },
    getters: {
        getSearchVal (state) {
            return state.searchVal;
        },
        getNavbarDropdownDisplay (state) {
            return state.navbarDropdownDisplay;
        }
    },
    mutations: {
        setSearchVal (state, newVal) {
            state.searchVal = newVal;
        },
        setNavbarDropdownDisplay (state, newVal) {
            state.navbarDropdownDisplay = newVal;
        }
    },
    actions: {}
});