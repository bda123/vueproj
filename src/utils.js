export {
    toCart,
    cartAddRemove,
    filter
}

function toCart(item) {
    let cart = JSON.parse(localStorage.getItem('cart'));
    if (cart) {
        if (!Object.keys(cart).includes(item.id)) {
            item.count = 1;
            cart[item.id] = item;
            localStorage.setItem('cart', JSON.stringify(cart));
        }
    } else {
        cart = {};
        item.count = 1;
        cart[item.id] = item;
        localStorage.setItem('cart', JSON.stringify(cart));
    }
}

function cartAddRemove(item, mode) {
    let cart = JSON.parse(localStorage.getItem('cart'));
    for (let [key, val] of Object.entries(cart)) {
        if (val.id === item.id) {
            if (mode === 'add') {
                val.count += 1;
            } else {
                val.count -= 1;
            }
        }
    }
    localStorage.setItem('cart', JSON.stringify(cart));
}

function filter (items, searchVal) {
    let filteredItems = [];
    for (let i in items) {
        if (items[i].name.toLowerCase().includes(searchVal.toLowerCase())) {
            filteredItems.push(items[i]);
        }
    }
    return filteredItems;
}